# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.9.4] - 20-11-2019
### Changed
* fix device state in filling purging frame

## [0.9.3] - 31-10-2019
### Changed
* changed device name for global hls
* fix order cell view

## [0.9.2] - 29-10-2019
### Changed
* Add taps management

## [0.9.0] - 01-10-2019
### Changed
* put it in gitlab
