# jAlignment

Application for ALGE group for alignment od ebs esrf Machine
Based on Icepap motors Hls system and AttributeCombiner

## Cloning

Any instructions or special flags required to clone the project. For example, if the project has submodules in git, then specify this and give the command to check them out correctly, example:

```
git clone --recurse-submodules git@gitlab.esrf.fr:accelerators/alignment/jalignment.git
```

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies

* Tango Controls 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.
* ATK-Core ATK-widget

#### Toolchain Dependencies

* javac

### Build

Instructions on building the project.

open the projcet in netBeans
add JTango.jar and ATKWidget.jar,ATKCore.jar s jar or library
and Build the project


