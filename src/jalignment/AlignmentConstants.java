/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jalignment;

import fr.esrf.tangoatk.widget.util.ErrorHistory;
import java.awt.Color;
import java.awt.Image;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import javax.swing.ImageIcon;

/**
 *
 * @author tappret
 */
public class AlignmentConstants {
    public final static Image ALGE_LOG = new ImageIcon(AlignmentConstants.class.getResource("resources/Niveau16px.png")).getImage();
    
    static final String SETTING_MANAGER_DEVICE_NAME = "sys/settings/alge-ref";
    
    static final String GIRDERS_ALL_DEVICE_NAME = "sr/a-jacks/all";
    static final String GIRDERS_POSITION_ATTR_NAME = "MoveRelatives";
    static final String GIRDERS_POSITION_REFERENCE_ATTR_NAME = "MoveRelativesRef";
    static final String GIRDERS_POSITION_DIFFERENCE_ATTR_NAME = "MoveRelativesDiff";
    
    static final String VALVES_SYNOPTIC_RESOURCE = "/jalignment/jdrawValves.jdw";
    static final String VALVES_FILL_PURGE_SYNOPTIC_RESOURCE = "/jalignment/jdrawFillingPurging.jdw";
    
    static final String CRATES_ALL_1_DEVICE_NAME = "sr/a-hls-crates/all-1";
    static final String CRATES_ALL_2_DEVICE_NAME = "sr/a-hls-crates/all-2";
    
    static final String MOTORS_ALL_DEVICE_NAME = "sr/a-motors-girders/all";
    static final String HLSD_ALL_DEVICE_NAME = "sr/a-hls/all";
    static final String HLSD_LEVELS_ATTR_NAME = "Levels";
    static final String HLSD_REFERENCE_ATTR_NAME = "LevelsRef";
    static final String HLSD_DIFFERENCE_ATTR_NAME = "LevelsDiff";
    
    static final String [] BASIC_ORDER_JACK_HLS = new String [] {"int1","int2","ext2","ext1"};
    
    
    static final String DEVICES_LIST_ATTR_NAME = "DeviceList";
    static final String DEFAULT_FORMAT_DATA = "%.3f";
    
    public static final NumberFormat cellFormat = new DecimalFormat("00");
    
    static final String[] LIST_CELL = makeCellList();
    static final double[] LIST_INDEX = makeIndexList();
    
    static final ErrorHistory errorHistory = new ErrorHistory();
    static final CellViewJFrame cellView = new CellViewJFrame();
    
    public static final String VERSION_NUMBER = "0.9.5";
    
    public static final Color LEVEL_COLOR = Color.BLUE;
    public static final Color REF_COLOR = Color.RED;
    public static final Color DIFF_COLOR = Color.ORANGE;
    
    public static String LAST_CELL_CLICKED = "";
    
    public static Color getColor(String fullAttrName){
        String attrName = extractAttributeName(fullAttrName);
        if(attrName.equals(HLSD_LEVELS_ATTR_NAME) || attrName.equals(GIRDERS_POSITION_ATTR_NAME)){
            return LEVEL_COLOR;
        }else if(attrName.equals(HLSD_REFERENCE_ATTR_NAME) || attrName.equals(GIRDERS_POSITION_REFERENCE_ATTR_NAME)){
            return REF_COLOR;
        }else if(attrName.equals(HLSD_DIFFERENCE_ATTR_NAME) || attrName.equals(GIRDERS_POSITION_DIFFERENCE_ATTR_NAME)){
            return DIFF_COLOR;
        }
        return null;
    }
    
    public static String extractMember(String name) {
        int idx = name.lastIndexOf('/');
        if (idx >= 0) {
            String n = name.substring(idx + 1);
            if (n.startsWith("cell")) {
                return "c" + n.substring(4);
            } else {
                return n;
            }
        } else {
            return "???";
        }
    }
    
    public static String extractAttributeName(String name) {
        int idx = name.lastIndexOf('/') + 1 ;
        if (idx >= 0) {
            return name.substring(idx, name.length());
        } else {
            return "???";
        }
    }
    
    static private String[] makeCellList(){
        String [] result = new String[32];
        for(int i = 1 ; i < 33;i++){
            result[i-1] = "c" + cellFormat.format(i);
        }
        return result;
    }
    
    static private double[] makeIndexList(){
        double [] result = new double[32];
        for(int i = 0 ; i < 32;i++){
            result[i] = i * 16;
            if(i >= 4 ){
                result[i] = result[i] + 4;
            }
        }
        return result;
    }
}
