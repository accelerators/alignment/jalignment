/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jalignment;

import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.AttributePolledList;
import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.INumberSpectrum;
import fr.esrf.tangoatk.core.ISpectrumListener;
import fr.esrf.tangoatk.core.IStringScalar;
import fr.esrf.tangoatk.core.NumberSpectrumEvent;
import fr.esrf.tangoatk.widget.util.chart.IJLChartListener;
import fr.esrf.tangoatk.widget.util.chart.JLChartEvent;
import fr.esrf.tangoatk.widget.util.chart.JLDataView;
import java.util.ArrayList;

/**
 *
 * @author tappret
 */
public class CellViewJFrame extends javax.swing.JFrame implements ISpectrumListener,IJLChartListener{

    LevelRefDiffView lrdv;
    boolean firstPlacement = true;
    ArrayList<INumberSpectrum> modelSubscribed = new ArrayList<>();
    AttributeList al = new AttributePolledList();
    int indexStart;
    int indexEnd;
    JLDataView dv;
    int cell = 1;
    boolean isHls;
    String [] axisLabel;

    /**
     * Creates new form CellViewJFramef
     */
    public CellViewJFrame() {
        initComponents();
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b); //To change body of generated methods, choose Tools | Templates.
        if (firstPlacement) {
            firstPlacement = false;
            this.setLocationRelativeTo(null);
        }
    }

    private String[] generateCellLabels(String cellLabel) {
        String[] labelList;
        int startGirder = 1;
        if (cellLabel.equals("c04")) {
            labelList = new String[20];
            startGirder = 0;
        } else {
            labelList = new String[16];
        }
        
        for (int girder = startGirder; girder < 5; girder++) {
            int idx = girder - startGirder;
            labelList[idx * 4]     = "g" + Integer.toString(girder) + AlignmentConstants.BASIC_ORDER_JACK_HLS[0];
            labelList[1 + idx * 4] = "g" + Integer.toString(girder) + AlignmentConstants.BASIC_ORDER_JACK_HLS[1];
            labelList[2 + idx * 4] = "g" + Integer.toString(girder) + AlignmentConstants.BASIC_ORDER_JACK_HLS[2];
            labelList[3 + idx * 4] = "g" + Integer.toString(girder) + AlignmentConstants.BASIC_ORDER_JACK_HLS[3];
        }
        return labelList ;
    }

    private double [] generateCellIndexs(String cellLabel){
        double [] indexList;
        if(cellLabel.equals("c04")){
            indexList = new double[20];
        } else {
            indexList = new double[16];
        }
        for(int i = 0; i < indexList.length ; i++){
            indexList[i]   = (double)(i);
        }
        return indexList;
    }
    
    public void setModel(LevelRefDiffView lrdv,String deviceName) throws ConnectionException{
        if(!modelSubscribed.isEmpty()){
            multiNumberSpectrumViewer.setJLChartListener(null);
            //do unsubscribe
            statusViewer1.clearModel();
            statusViewer2.clearModel();
            for(int i = 0 ; i < modelSubscribed.size() ; i++){
                INumberSpectrum ins = modelSubscribed.get(i);
                ins.removeSpectrumListener(this);
                multiNumberSpectrumViewer.removeNumberSpectrumModel(ins);
            }
            modelSubscribed.clear();
        }
        
        String cellStr = AlignmentConstants.extractMember(deviceName);
        if(!lrdv.isMotors){
            cellStr = cellStr.substring(0, 3);
        }
        cell = Integer.parseInt(cellStr.substring(1));
        
        isHls=false;
        if(deviceName.contains("hls-crate")){
            isHls=true;
            this.setTitle("Hls cell "+AlignmentConstants.cellFormat.format(cell));
            
            String baseDeviceName = deviceName.substring(0, deviceName.length()-2);
            
            statusViewer1.setModel((IStringScalar)al.add(baseDeviceName + "-1/status"));
            statusViewer2.setModel((IStringScalar)al.add(baseDeviceName + "-2/status"));
        }else{
            this.setTitle("Jacks cell "+AlignmentConstants.cellFormat.format(cell));
            
            statusViewer1.setModel((IStringScalar)al.add(deviceName + "/status"));
        }
        this.lrdv = lrdv;
        statusViewer2.setVisible(isHls);
        
        al.startRefresher();
        
        String [] devicesList = lrdv.getDeviceList();
        
        findIndexStartEndForData(devicesList,cellStr);
        
        INumberSpectrum [] insList = lrdv.getUsedModel();
        for(int i = 0 ; i < insList.length ; i++){
            insList[i].addSpectrumListener(this);
            modelSubscribed.add(insList[i]);
            multiNumberSpectrumViewer.addNumberSpectrumModel(insList[i]);
            multiNumberSpectrumViewer.getDataView(insList[i]).setColor(AlignmentConstants.getColor(insList[i].getName()));
            insList[i].refresh();
        }
        axisLabel = generateCellLabels(cellStr);
        
        multiNumberSpectrumViewer.getXAxis().setLabels(axisLabel, generateCellIndexs(cellStr));
        multiNumberSpectrumViewer.setJLChartListener(this);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        multiNumberSpectrumViewer = new MultiNumberSpectrumViewerCustom();
        jPanel1 = new javax.swing.JPanel();
        statusViewer1 = new fr.esrf.tangoatk.widget.attribute.StatusViewer();
        statusViewer2 = new fr.esrf.tangoatk.widget.attribute.StatusViewer();

        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });

        jSplitPane1.setDividerLocation(200);
        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        multiNumberSpectrumViewer.setPreferredSize(new java.awt.Dimension(0, 400));
        jSplitPane1.setTopComponent(multiNumberSpectrumViewer);

        jPanel1.setPreferredSize(new java.awt.Dimension(1128, 1000));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(statusViewer1, javax.swing.GroupLayout.DEFAULT_SIZE, 325, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusViewer2, javax.swing.GroupLayout.DEFAULT_SIZE, 325, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusViewer1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(statusViewer2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jSplitPane1.setRightComponent(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 658, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 346, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
        jSplitPane1.setDividerLocation(0.8);
    }//GEN-LAST:event_formComponentResized

    /**
     * @param args the command line arguments
     */
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CellViewJFrame



.class  


.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CellViewJFrame



.class  


.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CellViewJFrame



.class  


.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CellViewJFrame



.class  


.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CellViewJFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSplitPane jSplitPane1;
    private fr.esrf.tangoatk.widget.attribute.MultiNumberSpectrumViewer multiNumberSpectrumViewer;
    private fr.esrf.tangoatk.widget.attribute.StatusViewer statusViewer1;
    private fr.esrf.tangoatk.widget.attribute.StatusViewer statusViewer2;
    // End of variables declaration//GEN-END:variables

    @Override
    public void spectrumChange(NumberSpectrumEvent nse) {
        double[] allValues = nse.getValue();
        double[] value = new double[indexEnd-indexStart+1];
        System.arraycopy(allValues, indexStart, value, 0, value.length);
        
        JLDataView dv = multiNumberSpectrumViewer.getDataView((INumberSpectrum)nse.getSource());
        dv.reset();
        for(int i = 0 ; i < value.length ; i++){
            dv.add((double)i,value[i]);
        }
        multiNumberSpectrumViewer.repaint();
    }

    @Override
    public void stateChange(AttributeStateEvent ase) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void errorChange(ErrorEvent ee) {
        AlignmentConstants.errorHistory.setErrorOccured(ee);
    }

    private void findIndexStartEndForData(String[] devicesList,String cell) {
        indexStart = -1;
        indexEnd = -1;
        for(int i = 0 ; i < devicesList.length ; i++){
            if(devicesList[i].contains(cell)){
                indexStart = i;
                break;
            }
        }
        for(int i = (devicesList.length-1) ; i >= 0 ; i--){
            if(devicesList[i].contains(cell)){
                indexEnd = i;
                break;
            }
        }
    }

    @Override
    public String[] clickOnChart(JLChartEvent evt) {
        String attrName = AlignmentConstants.extractAttributeName(evt.getDataView().getName());
        double x = evt.getXValue();
        double y = evt.getYValue();
        
        int xInt = (int)x;
        
        String[] str = new String[2];
        str[0] = attrName + " " + axisLabel[xInt];
        str[1] = String.format(AlignmentConstants.DEFAULT_FORMAT_DATA, y) + " mm";
        return str;
    }

    
}
