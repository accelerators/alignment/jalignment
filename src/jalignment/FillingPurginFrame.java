/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jalignment;

import fr.esrf.tangoatk.core.ConnectionException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.MissingResourceException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 * @author tappret
 */
public class FillingPurginFrame extends javax.swing.JFrame {

    /**
     * Creates new form FillingPurginFrame
     */
    public FillingPurginFrame() {
        initComponents();
        
        try {
            this.setTitle("Filling / Purgin");
            
            synopticFileViewer1.setAutoZoom(true);

            synopticFileViewer1.setErrorHistoryWindow(AlignmentConstants.errorHistory);
            synopticFileViewer1.loadSynopticFromStream(new InputStreamReader(getClass().getResourceAsStream(AlignmentConstants.VALVES_FILL_PURGE_SYNOPTIC_RESOURCE)));
            this.setLocationRelativeTo(null);
            this.pack();
        } catch (IOException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MissingResourceException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        synopticFileViewer1 = new fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        synopticFileViewer1.setMinimumSize(new java.awt.Dimension(200, 100));
        getContentPane().add(synopticFileViewer1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FillingPurginFrame fpf = new FillingPurginFrame();
                fpf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                fpf.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer synopticFileViewer1;
    // End of variables declaration//GEN-END:variables
}
