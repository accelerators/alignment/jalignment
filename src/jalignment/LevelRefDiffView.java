/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jalignment;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.DeviceFactory;
import fr.esrf.tangoatk.core.INumberSpectrum;
import fr.esrf.tangoatk.core.IStringSpectrum;
import fr.esrf.tangoatk.widget.attribute.MultiNumberSpectrumViewer;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.chart.IJLChartListener;
import fr.esrf.tangoatk.widget.util.chart.JLChartEvent;
import fr.esrf.tangoatk.widget.util.chart.JLDataView;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.AttributedCharacterIterator;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;

/**
 *
 * @author tappret
 */
public class LevelRefDiffView implements ActionListener,IJLChartListener {
    JCheckBox levelCheckBox, refCheckBox, diffCheckBox;
    MultiNumberSpectrumViewer multiNumberViewer;
    boolean isMotors;
    
    private INumberSpectrum insLevel;
    private INumberSpectrum insRef;
    private INumberSpectrum insDiff;
    private IStringSpectrum issDevName;

    /**
     * @param JCheckBox[] levelCheckBox,refCheckBox,diffCheckBox
     */
    public LevelRefDiffView(JCheckBox[] levelRefDiff,String [] attributeListName,AttributeList al, MultiNumberSpectrumViewer multiNumberViewer,boolean isMotors) throws ConnectionException {
        this.levelCheckBox = levelRefDiff[0];
        this.refCheckBox = levelRefDiff[1];
        this.diffCheckBox = levelRefDiff[2];
        this.multiNumberViewer = multiNumberViewer;
        this.isMotors = isMotors;

        insLevel = (INumberSpectrum) al.add(attributeListName[0]);
        insRef = (INumberSpectrum) al.add(attributeListName[1]);
        insDiff = (INumberSpectrum) al.add(attributeListName[2]);
        issDevName = (IStringSpectrum) al.add(attributeListName[3]);

        Font f = multiNumberViewer.getHeaderFont();
        multiNumberViewer.setHeaderFont(f.deriveFont(Font.BOLD));
        if(isMotors){
            multiNumberViewer.setHeader("Motors");
        }else{
            multiNumberViewer.setHeader("Hls");
        }

        multiNumberViewer.addNumberSpectrumModel(insLevel,"level");
        multiNumberViewer.getDataView(insLevel).setColor(AlignmentConstants.LEVEL_COLOR);
        multiNumberViewer.addNumberSpectrumModel(insRef,"ref");
        multiNumberViewer.getDataView(insLevel).setColor(AlignmentConstants.REF_COLOR);
        
        multiNumberViewer.setJLChartListener(this);
        multiNumberViewer.getXAxis().setLabels(AlignmentConstants.LIST_CELL, AlignmentConstants.LIST_INDEX);
        
        for(JCheckBox checkBox : levelRefDiff){
            checkBox.addActionListener(this);
        }
        actionPerformed(null);
    }
    
    public INumberSpectrum [] getUsedModel(){
        ArrayList<INumberSpectrum> modelUsed = new ArrayList<>();
        
        if(diffCheckBox.isSelected()){
            modelUsed.add(insDiff);
        }
        
        if (refCheckBox.isSelected()){
            modelUsed.add(insRef);
        }
        
        if (levelCheckBox.isSelected()){
            modelUsed.add(insLevel);
        }
        return (INumberSpectrum [])modelUsed.toArray(new INumberSpectrum[modelUsed.size()]);
    }
    
    public String [] getDeviceList(){
        return issDevName.getStringSpectrumValue();
    }

    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(diffCheckBox.isSelected()){
            refCheckBox.setSelected(false);
            levelCheckBox.setSelected(false);
        }else{
            diffCheckBox.setSelected(false);
        }
        
        if(!diffCheckBox.isSelected() && !levelCheckBox.isSelected() && !refCheckBox.isSelected()){
            refCheckBox.setSelected(true);
            levelCheckBox.setSelected(true);
        }
        
        multiNumberViewer.removeNumberSpectrumModel(insDiff);
        multiNumberViewer.removeNumberSpectrumModel(insLevel);
        multiNumberViewer.removeNumberSpectrumModel(insRef);
        
        if(diffCheckBox.isSelected()){
            multiNumberViewer.addNumberSpectrumModel(insDiff);
            JLDataView jldv = multiNumberViewer.getDataView(insDiff);
            jldv.setColor(AlignmentConstants.DIFF_COLOR);
        }
        
        if (refCheckBox.isSelected()){
            multiNumberViewer.addNumberSpectrumModel(insRef);
            JLDataView jldv = multiNumberViewer.getDataView(insRef);
            jldv.setColor(AlignmentConstants.REF_COLOR);
        }
        
        if (levelCheckBox.isSelected()){
            multiNumberViewer.addNumberSpectrumModel(insLevel);
            JLDataView jldv = multiNumberViewer.getDataView(insLevel);
            jldv.setColor(AlignmentConstants.LEVEL_COLOR);
        }
        
        //refresh Model in cellView
        if(AlignmentConstants.cellView.isVisible()){
            try {
                AlignmentConstants.cellView.setModel(this, AlignmentConstants.LAST_CELL_CLICKED);
            } catch (ConnectionException ex) {
                Logger.getLogger(LevelRefDiffView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
            
    }

    @Override
    public String[] clickOnChart(JLChartEvent jlce) {
        String fullAttrName = jlce.getDataView().getName();
        String attrName = AlignmentConstants.extractAttributeName(fullAttrName);
        
        double value = jlce.getYValue();
        int indexX = (int)jlce.getXValue();
        String[] toolTips = new String[3];
        String devName = issDevName.getStringSpectrumValue()[indexX];
        String member = AlignmentConstants.extractMember(devName); 
        String cell = member.substring(1,3);
        String girder = member.substring(4,5);
        String intExt = member.substring(5,8);
        String frontBack = member.substring(8,9);
        if(intExt.contains("int")){
            intExt = "Interior";
        }else{
            intExt = "Exterior";
        }
        
        if(isMotors){
            toolTips[0] = attrName + " of Jack ";
        }else{
            toolTips[0] = attrName + " of Hls ";
        }
        
        toolTips[0] += "Cell"+cell + " Girder "+girder + " "+intExt + " "+ frontBack;
        toolTips[1] = fullAttrName;
        toolTips[2] = String.format(AlignmentConstants.DEFAULT_FORMAT_DATA, value) + " mm";
        return toolTips;
    }
    
    /**
     *
     */
    public void saveReference(){
        try {
            DeviceProxy dp;
            DeviceData dd = new DeviceData();
            if(isMotors){
                dp = DeviceFactory.getInstance().getDevice(AlignmentConstants.GIRDERS_ALL_DEVICE_NAME);
                dd.insert(AlignmentConstants.GIRDERS_POSITION_ATTR_NAME);
            } else {
                dp = DeviceFactory.getInstance().getDevice(AlignmentConstants.HLSD_ALL_DEVICE_NAME);
                dd.insert(AlignmentConstants.HLSD_LEVELS_ATTR_NAME);
            }
            dp.command_inout("saveAttribute",dd);
        } catch (Exception ex) {
            if(isMotors){
                ErrorPane.showErrorMessage(this.multiNumberViewer, AlignmentConstants.GIRDERS_ALL_DEVICE_NAME, ex);
            }else{
                ErrorPane.showErrorMessage(this.multiNumberViewer, AlignmentConstants.HLSD_ALL_DEVICE_NAME, ex);
            }
        }
    }

    private INumberSpectrum getModelFromAttrName(String attrName) {
        if(isMotors){
            if(attrName.contains(AlignmentConstants.GIRDERS_POSITION_ATTR_NAME)){
                return insLevel;
            }else if(attrName.contains(AlignmentConstants.GIRDERS_POSITION_REFERENCE_ATTR_NAME)){
                return insRef;
            }else{
                return insDiff;
            }
        }else{
            if(attrName.contains(AlignmentConstants.HLSD_LEVELS_ATTR_NAME)){
                return insLevel;
            }else if(attrName.contains(AlignmentConstants.HLSD_REFERENCE_ATTR_NAME)){
                return insRef;
            }else{
                return insDiff;
            }
        }
    }
    
}
