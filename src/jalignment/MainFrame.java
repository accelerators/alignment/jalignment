/*z
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jalignment;

import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.AttributePolledList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.INumberSpectrum;
import fr.esrf.tangoatk.core.IStringSpectrum;
import fr.esrf.tangoatk.widget.util.LoadSaveFileListener;
import fr.esrf.tangoatk.widget.util.SettingsManagerProxy;
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.MissingResourceException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;

/**
 *
 * @author tappret
 */
public class MainFrame extends javax.swing.JFrame implements LoadSaveFileListener{
        
    private SettingsManagerProxy smProxy = null;
    
    LevelRefDiffView motorLevelView;
    LevelRefDiffView hlsLevelView;
    
    FillingPurginFrame fpf = new FillingPurginFrame();
    
    /**
     * Creates new form MainFrames
     */
    public MainFrame() {
        initComponents();
        
        setTitle("jAlignment  " + AlignmentConstants.VERSION_NUMBER);
        setIconImage(AlignmentConstants.ALGE_LOG);
        
        try {
            AttributeList al = new AttributePolledList();
            al.addErrorListener(AlignmentConstants.errorHistory);
            
            synopticFileViewer1.setAutoZoom(true);
            
            synopticFileViewer1.setErrorHistoryWindow(AlignmentConstants.errorHistory);
            synopticFileViewer1.loadSynopticFromStream(new InputStreamReader(getClass().getResourceAsStream(AlignmentConstants.VALVES_SYNOPTIC_RESOURCE)));
            
            String [] attrListMotor = {AlignmentConstants.GIRDERS_ALL_DEVICE_NAME + "/" + AlignmentConstants.GIRDERS_POSITION_ATTR_NAME,
                                    AlignmentConstants.GIRDERS_ALL_DEVICE_NAME + "/" + AlignmentConstants.GIRDERS_POSITION_REFERENCE_ATTR_NAME,
                                    AlignmentConstants.GIRDERS_ALL_DEVICE_NAME + "/" + AlignmentConstants.GIRDERS_POSITION_DIFFERENCE_ATTR_NAME,
                                    AlignmentConstants.GIRDERS_ALL_DEVICE_NAME + "/" + AlignmentConstants.DEVICES_LIST_ATTR_NAME};
            
            JCheckBox [] checkBoxListMotors = {jCheckBoxMotorsLevel,jCheckBoxMotorsReferences,jCheckBoxMotorsDifferences};
            
            motorLevelView = new LevelRefDiffView(checkBoxListMotors, attrListMotor, al, multiNumberSpectrumViewerMotors,true);
            
            String [] attrListHls = {AlignmentConstants.HLSD_ALL_DEVICE_NAME + "/" + AlignmentConstants.HLSD_LEVELS_ATTR_NAME,
                                    AlignmentConstants.HLSD_ALL_DEVICE_NAME + "/" + AlignmentConstants.HLSD_REFERENCE_ATTR_NAME,
                                    AlignmentConstants.HLSD_ALL_DEVICE_NAME + "/" + AlignmentConstants.HLSD_DIFFERENCE_ATTR_NAME,
                                    AlignmentConstants.HLSD_ALL_DEVICE_NAME + "/" + AlignmentConstants.DEVICES_LIST_ATTR_NAME};
            
            JCheckBox [] checkBoxListHls = {jCheckBoxHlsLevel,jCheckBoxHlsReference,jCheckBoxHlsDifferences};
            
            hlsLevelView = new LevelRefDiffView(checkBoxListHls, attrListHls, al, multiNumberSpectrumViewerHls,false);
            
            al.startRefresher();
            
        } catch (ConnectionException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MissingResourceException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        jMenuItemErrorHistory.setText("Error History ...");
        jMenuItemErrorHistory.addActionListener((java.awt.event.ActionEvent evt) -> {
            AlignmentConstants.errorHistory.setVisible(true);
        });

        jMenuItemAtkDiagnostic.setText("Diagnostic ...");
        jMenuItemAtkDiagnostic.addActionListener((java.awt.event.ActionEvent evt) -> {
            fr.esrf.tangoatk.widget.util.ATKDiagnostic.showDiagnostic();
        });
        
        SpectrumStatePanel crates1 = new SpectrumStatePanel(this, AlignmentConstants.CRATES_ALL_1_DEVICE_NAME);
        SpectrumStatePanel crates2 = new SpectrumStatePanel(this, AlignmentConstants.CRATES_ALL_2_DEVICE_NAME);
        crates1.startRefresher();
        crates2.startRefresher();
        jPanelTop.setLayout(new BorderLayout());
        jPanelBottom.setLayout(new BorderLayout());
        
        jPanelTop.add(crates1,BorderLayout.CENTER);
        jPanelBottom.add(crates2,BorderLayout.CENTER);
        
        SpectrumStatePanel motors = new SpectrumStatePanel(this, AlignmentConstants.MOTORS_ALL_DEVICE_NAME);
        motors.startRefresher();
        jPanelMotors.setLayout(new BorderLayout());
        jPanelMotors.add(motors,BorderLayout.CENTER);
        
        addSettingsManagerSupport();
        
        this.pack();
        
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                jSplitPane1.setDividerLocation(0.5);
            }
        });
        
        this.setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel2 = new javax.swing.JPanel();
        multiNumberSpectrumViewerMotors = new fr.esrf.tangoatk.widget.attribute.MultiNumberSpectrumViewer();
        jCheckBoxMotorsDifferences = new javax.swing.JCheckBox();
        jCheckBoxMotorsReferences = new javax.swing.JCheckBox();
        jCheckBoxMotorsLevel = new javax.swing.JCheckBox();
        jPanelMotors = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        multiNumberSpectrumViewerHls = new fr.esrf.tangoatk.widget.attribute.MultiNumberSpectrumViewer();
        jPanelValvesSynoptic = new javax.swing.JPanel();
        synopticFileViewer1 = new fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanelTop = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanelBottom = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jCheckBoxHlsLevel = new javax.swing.JCheckBox();
        jCheckBoxHlsReference = new javax.swing.JCheckBox();
        jCheckBoxHlsDifferences = new javax.swing.JCheckBox();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItemPreview = new javax.swing.JMenuItem();
        jMenuItemLoad = new javax.swing.JMenuItem();
        jMenuItemSave = new javax.swing.JMenuItem();
        jMenuItemQuit = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItemErrorHistory = new javax.swing.JMenuItem();
        jMenuItemAtkDiagnostic = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1174, 600));

        jSplitPane1.setDividerLocation(200);
        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        jCheckBoxMotorsDifferences.setText("Show Differences");

        jCheckBoxMotorsReferences.setSelected(true);
        jCheckBoxMotorsReferences.setText("Show References");

        jCheckBoxMotorsLevel.setSelected(true);
        jCheckBoxMotorsLevel.setText("Show Positions");

        javax.swing.GroupLayout jPanelMotorsLayout = new javax.swing.GroupLayout(jPanelMotors);
        jPanelMotors.setLayout(jPanelMotorsLayout);
        jPanelMotorsLayout.setHorizontalGroup(
            jPanelMotorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanelMotorsLayout.setVerticalGroup(
            jPanelMotorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 23, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(multiNumberSpectrumViewerMotors, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(682, Short.MAX_VALUE)
                .addComponent(jCheckBoxMotorsLevel)
                .addGap(18, 18, 18)
                .addComponent(jCheckBoxMotorsReferences)
                .addGap(18, 18, 18)
                .addComponent(jCheckBoxMotorsDifferences)
                .addContainerGap())
            .addComponent(jPanelMotors, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(multiNumberSpectrumViewerMotors, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelMotors, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxMotorsDifferences)
                    .addComponent(jCheckBoxMotorsReferences)
                    .addComponent(jCheckBoxMotorsLevel)))
        );

        jSplitPane1.setTopComponent(jPanel2);

        jPanelValvesSynoptic.setMaximumSize(new java.awt.Dimension(2147483647, 50));
        jPanelValvesSynoptic.setMinimumSize(new java.awt.Dimension(0, 25));
        jPanelValvesSynoptic.setPreferredSize(new java.awt.Dimension(2500, 25));
        jPanelValvesSynoptic.setLayout(new java.awt.BorderLayout());

        synopticFileViewer1.setToolTipText("");
        synopticFileViewer1.setAutoZoom(true);
        synopticFileViewer1.setLayout(new java.awt.BorderLayout());
        jPanelValvesSynoptic.add(synopticFileViewer1, java.awt.BorderLayout.CENTER);

        jLabel1.setText("1");

        javax.swing.GroupLayout jPanelTopLayout = new javax.swing.GroupLayout(jPanelTop);
        jPanelTop.setLayout(jPanelTopLayout);
        jPanelTopLayout.setHorizontalGroup(
            jPanelTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanelTopLayout.setVerticalGroup(
            jPanelTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jLabel2.setText("2");

        javax.swing.GroupLayout jPanelBottomLayout = new javax.swing.GroupLayout(jPanelBottom);
        jPanelBottom.setLayout(jPanelBottomLayout);
        jPanelBottomLayout.setHorizontalGroup(
            jPanelBottomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanelBottomLayout.setVerticalGroup(
            jPanelBottomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelTop, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelBottom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelTop, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelBottom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jCheckBoxHlsLevel.setSelected(true);
        jCheckBoxHlsLevel.setText("Show Positions");

        jCheckBoxHlsReference.setSelected(true);
        jCheckBoxHlsReference.setText("Show References");

        jCheckBoxHlsDifferences.setText("Show Differences");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .addComponent(jCheckBoxHlsLevel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBoxHlsReference)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBoxHlsDifferences))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jCheckBoxHlsReference)
                .addComponent(jCheckBoxHlsLevel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jCheckBoxHlsDifferences))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 690, Short.MAX_VALUE)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jPanelValvesSynoptic, javax.swing.GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE)
            .addComponent(multiNumberSpectrumViewerHls, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(multiNumberSpectrumViewerHls, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelValvesSynoptic, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jSplitPane1.setRightComponent(jPanel3);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jSplitPane1)
                .addGap(0, 0, 0))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );

        jMenu1.setText("File");

        jMenuItemPreview.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemPreview.setText("Preview ...");
        jMenuItemPreview.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemPreviewActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemPreview);

        jMenuItemLoad.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemLoad.setText("Load References ...");
        jMenuItemLoad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemLoadActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemLoad);

        jMenuItemSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemSave.setText("Save References ...");
        jMenuItemSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemSaveActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemSave);

        jMenuItemQuit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        jMenuItemQuit.setText("Quit");
        jMenuItemQuit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemQuitActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemQuit);

        jMenuBar1.add(jMenu1);

        jMenu3.setText("View");

        jMenuItemErrorHistory.setText("Error History ...");
        jMenu3.add(jMenuItemErrorHistory);

        jMenuItemAtkDiagnostic.setText("Diagnostic ...");
        jMenu3.add(jMenuItemAtkDiagnostic);

        jMenuBar1.add(jMenu3);

        jMenu4.setText("Expert");

        jMenuItem3.setText("Filling / Purging");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem3);

        jMenuItem1.setText("Save jacks positons");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem1);

        jMenuItem2.setText("Save Hls positions");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem2);

        jMenuBar1.add(jMenu4);

        jMenu5.setText("About");

        jMenuItem4.setText("Changelog");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem4);

        jMenuItem5.setText("Version");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem5);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemQuitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemQuitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItemQuitActionPerformed

    private void jMenuItemSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemSaveActionPerformed
        smProxy.saveSettingsFile();
    }//GEN-LAST:event_jMenuItemSaveActionPerformed

    private void jMenuItemLoadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemLoadActionPerformed
        smProxy.loadSettingsFile();
    }//GEN-LAST:event_jMenuItemLoadActionPerformed

    private void jMenuItemPreviewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemPreviewActionPerformed
        smProxy.previewSettingsFile();
    }//GEN-LAST:event_jMenuItemPreviewActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        motorLevelView.saveReference();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        hlsLevelView.saveReference();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        Rectangle bounds = this.getBounds();
        int marging = 32;
        Rectangle fillPurgBounds = fpf.getBounds();
        
        fpf.setLocation(bounds.x + bounds.width/2 - fillPurgBounds.width/2, bounds.height + bounds.y + marging);
        fpf.setVisible(true);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        String message = "Java Version : " + System.getProperty("java.version") + "\n" + 
                MainFrame.class.getPackage().getName() + " Version : " + AlignmentConstants.VERSION_NUMBER;
        
        JOptionPane.showMessageDialog(rootPane, message, "Version", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        try {
            Desktop desktop = java.awt.Desktop.getDesktop();
            URI oURL = new URI("https://gitlab.esrf.fr/accelerators/alignment/jalignment/blob/master/CHANGELOG.md");
            desktop.browse(oURL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new MainFrame().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox jCheckBoxHlsDifferences;
    private javax.swing.JCheckBox jCheckBoxHlsLevel;
    private javax.swing.JCheckBox jCheckBoxHlsReference;
    private javax.swing.JCheckBox jCheckBoxMotorsDifferences;
    private javax.swing.JCheckBox jCheckBoxMotorsLevel;
    private javax.swing.JCheckBox jCheckBoxMotorsReferences;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItemAtkDiagnostic;
    private javax.swing.JMenuItem jMenuItemErrorHistory;
    private javax.swing.JMenuItem jMenuItemLoad;
    private javax.swing.JMenuItem jMenuItemPreview;
    private javax.swing.JMenuItem jMenuItemQuit;
    private javax.swing.JMenuItem jMenuItemSave;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanelBottom;
    private javax.swing.JPanel jPanelMotors;
    private javax.swing.JPanel jPanelTop;
    private javax.swing.JPanel jPanelValvesSynoptic;
    private javax.swing.JSplitPane jSplitPane1;
    private fr.esrf.tangoatk.widget.attribute.MultiNumberSpectrumViewer multiNumberSpectrumViewerHls;
    private fr.esrf.tangoatk.widget.attribute.MultiNumberSpectrumViewer multiNumberSpectrumViewerMotors;
    private fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer synopticFileViewer1;
    // End of variables declaration//GEN-END:variables

    
    private void addSettingsManagerSupport()
    {
        smProxy = new SettingsManagerProxy(AlignmentConstants.SETTING_MANAGER_DEVICE_NAME);
        String username = System.getProperty("user.name");
        String author = "JAlignment ("+ username + ")";
        smProxy.setFileAuthor(author);
        smProxy.setErrorHistoryWindow(AlignmentConstants.errorHistory);
        //JPanel smPanel = smProxy.getSettingsPanel();
        smProxy.settingsPanelHideChild(SettingsManagerProxy.FILE_LABEL);
        smProxy.setLoadSaveListener(this);
        //jPanel3.add(smPanel,BorderLayout.CENTER);
    }

    @Override
    public void beforeLoad(SettingsManagerProxy src, File f) {
        
    }

    @Override
    public void afterLoad(SettingsManagerProxy src, File f) {
        
    }

    @Override
    public void beforeSave(SettingsManagerProxy src, File f) {
        this.hlsLevelView.saveReference();
        this.motorLevelView.saveReference();
    }

    @Override
    public void afterSave(SettingsManagerProxy src, File f) {
        
    }
}
