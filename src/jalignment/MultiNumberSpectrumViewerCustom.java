/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jalignment;

import fr.esrf.tangoatk.core.INumberSpectrum;
import fr.esrf.tangoatk.widget.attribute.MultiNumberSpectrumViewer;
import fr.esrf.tangoatk.widget.util.chart.JLDataView;

/**
 *
 * @author tappret
 */
public class MultiNumberSpectrumViewerCustom extends MultiNumberSpectrumViewer{

    @Override
    public void addNumberSpectrumModel(INumberSpectrum ins) {
        String attFormat = null;
        JLDataView attDvy = null;
        int orderNumber = 0;

        if (ins == null) return;

        if (attMap.containsKey(ins)) return;

        orderNumber = attMap.size();

        attFormat = ins.getFormat();
        attDvy = new JLDataView();
        attDvy.setUserFormat(attFormat);
        attDvy.setUnit(ins.getUnit());
        attDvy.setName(ins.getName());
        attDvy.setColor(defColors[orderNumber % defColors.length]);
        if (defaultAxis.equalsIgnoreCase(AXIS_Y1))
          getY1Axis().addDataView(attDvy);
        else
          getY2Axis().addDataView(attDvy);

        attMap.put(ins, attDvy);
    }
    
    
}
