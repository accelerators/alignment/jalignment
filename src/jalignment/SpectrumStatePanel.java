package jalignment;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.core.*;
import fr.esrf.tangoatk.widget.util.ATKConstant;
import fr.esrf.tangoatk.widget.util.ErrorPane;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A panel to show steerer states
 */
class SingleStatePanel extends JPanel{
    

    private JButton stState;
    String devName = null;
    SpectrumStatePanel parentComponent;
    int index = -1;


    SingleStatePanel(SpectrumStatePanel parentComponent, int index) {
        this.parentComponent = parentComponent;
        this.index = index;
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEtchedBorder());

        JPanel stPanel = new JPanel();
        stPanel.setLayout(new BorderLayout());
        stPanel.setBorder(BorderFactory.createEmptyBorder(2, 3, 2, 3));
        add(stPanel, BorderLayout.CENTER);

        stState = new JButton();
        Font myFont = ATKConstant.labelFont;
        myFont = myFont.deriveFont(Font.BOLD, 10);
        stState.setFont(myFont);
        stState.setMargin(new Insets(0, 0, 0, 0));
        
        stState.setBackground(ATKConstant.getColor4State(IDevice.UNKNOWN));
        stPanel.add(stState, BorderLayout.CENTER);
        stState.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    AlignmentConstants.cellView.setVisible(false);
                    if(parentComponent.isMotors){
                        AlignmentConstants.cellView.setModel(parentComponent.parentFrame.motorLevelView, devName);
                    }else{
                        AlignmentConstants.cellView.setModel(parentComponent.parentFrame.hlsLevelView, devName);
                    }
                    AlignmentConstants.LAST_CELL_CLICKED = devName;
                    AlignmentConstants.cellView.setVisible(true);
                }catch (ConnectionException ce) {
                    ErrorPane.showErrorMessage(parentComponent.parentFrame, devName, ce);
                }
            }
        });
    }
    
    String [] addToStartArray(String [] array, String toAdd){
        String [] newArray = new String[ array.length + 1 ];
        newArray[0] = toAdd;
        System.arraycopy(array, 0, newArray, 1, array.length);
        return newArray;
    }
    
    String [] addToEndArray(String [] array, String toAdd){
        String [] newArray = new String[ array.length + 1 ];
        newArray[array.length] = toAdd;
        System.arraycopy(array, 0, newArray, 0, array.length);
        return newArray;
    }
    
    String [] removeFormArray(String [] array, String toRemove){
        String [] newArray = new String[ array.length - 1 ];
        int offset = 0;
        for(int i = 0; i < array.length;i++){
            if(array[i].equalsIgnoreCase(toRemove)){
                offset = -1;
            }else{
                newArray[i + offset] = array[i];
            }
        }
        return newArray;
    }

    void updateState(String state) {
        stState.setBackground(ATKConstant.getColor4State(state));
    }
    
    void updateDeviceName(String text) {
        devName = text;
        text = AlignmentConstants.extractMember(text);
        stState.setToolTipText(devName);
        if(!parentComponent.isMotors){
            text = text.substring(0,text.length()-2);
        }
        stState.setText(text);
    }

}

public class SpectrumStatePanel extends JPanel implements IDevStateSpectrumListener,IStringSpectrumListener {

    final static int MAX_ROW_NUMBER = 1;
    private AttributeList attList;

    private JPanel stPanel;
    private ArrayList<SingleStatePanel> stPanels;

    private IDevStateSpectrum stModel = null;
    private IStringSpectrum devNameModel = null;
    GridLayout mainGrid;
    MainFrame parentFrame;
    String deviceNameCombiner;
    boolean isMotors = false;
    
    SpectrumStatePanel(MainFrame parentFrame,String deviceNameCombiner) {
        this.parentFrame = parentFrame;
        this.deviceNameCombiner = deviceNameCombiner;
        if(deviceNameCombiner == AlignmentConstants.MOTORS_ALL_DEVICE_NAME){
            isMotors = true;
        }
        
        attList = new AttributeList();
        attList.addErrorListener(AlignmentConstants.errorHistory);

        setLayout(new BorderLayout());

        stPanel = new JPanel();
        mainGrid = new GridLayout(1, 1);
        stPanel.setLayout(mainGrid);

        add(stPanel, BorderLayout.CENTER);
        
        stPanels = new ArrayList<>();

        try {

            stModel = (IDevStateSpectrum) attList.add(deviceNameCombiner + "/States");
            stModel.addDevStateSpectrumListener((IDevStateSpectrumListener) this);
            
            devNameModel = (IStringSpectrum) attList.add(deviceNameCombiner + "/DeviceList");
            devNameModel.addListener((IStringSpectrumListener) this);

        } catch (ConnectionException e) {
        }
        
        attList.setRefreshInterval(1000);
        attList.stopRefresher();

    }

    @Override
    public void devStateSpectrumChange(DevStateSpectrumEvent evt) {
        Object src = evt.getSource();

        if (src == stModel) {
            String[] st = evt.getValue();
            int delta = st.length - stPanels.size();
            if(delta != 0){
                adaptePanels(delta);
            }
            
            for (int i = 0; i < stPanels.size(); i++) {
                stPanels.get(i).updateState(st[i]);
            }
        }

    }

    @Override
    public void stateChange(AttributeStateEvent evt) {
        evt.getSource();
    }

    @Override
    public void errorChange(ErrorEvent evt) {

        Object src = evt.getSource();

        if (src == stModel) {
            for (int i = 0; i < stPanels.size(); i++) {
                stPanels.get(i).updateState(IDevice.UNKNOWN);
            }
        }
    }

    public void clearModel() {

        if (stModel != null) {
            stModel.removeDevStateSpectrumListener(this);
        }
        stModel = null;

        //attList.removeErrorListener(MainPanel.errWin);
        attList.stopRefresher();
        attList = null;
        setVisible(false);

    }

    public void startRefresher() {
        attList.startRefresher();
    }

    public void stopRefresher() {
        attList.stopRefresher();
    }

    public static void main(String args[]) {

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                
                MainFrame mf = new MainFrame();
                SpectrumStatePanel st = new SpectrumStatePanel(mf,"sr/a-hls-crates/all-1");
                st.startRefresher();
                mf.setContentPane(st);
                mf.pack();
                mf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

                mf.setVisible(true);
            }
        });
    }

    private void adaptePanels(int delta) {
        int oldSize = stPanels.size();
        /**/

        DeviceProxy ds;
        try {
            ds = new DeviceProxy(deviceNameCombiner);

            DeviceAttribute attData = ds.read_attribute("DeviceList");
            String[] names = attData.extractStringArray();

            if (names.length < MAX_ROW_NUMBER) {
                mainGrid.setRows(names.length);
            } else {
                int nbColumn = (names.length / MAX_ROW_NUMBER) + 1;
                mainGrid.setRows(MAX_ROW_NUMBER);
                mainGrid.setColumns(nbColumn);
            }

        /**/
        
        
            if (delta > 0) {
                for (int i = 0; i < delta; i++) {
                    stPanels.add(new SingleStatePanel(this,i));
                    stPanel.add(stPanels.get(i+oldSize));
                }
            } else if (delta < 0){
                delta = Math.abs(delta);
                for (int i = 1; i < delta + 1; i++) {
                    stPanel.remove(stPanels.get(oldSize-i));
                    stPanels.remove(stPanels.get(oldSize-i));
                }
            }
            parentFrame.pack();
            

        } catch (DevFailed ex) {
            Logger.getLogger(SpectrumStatePanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getState(int index){
        return stModel.getValue()[index];
    }

    @Override
    public void stringSpectrumChange(StringSpectrumEvent sse) {
        Object src = sse.getSource();

        if (src == devNameModel) {
            String[] devName = sse.getValue();
            int delta = devName.length - stPanels.size();
            if(delta != 0){
                adaptePanels(delta);
            }
            
            for (int i = 0; i < stPanels.size(); i++) {
                stPanels.get(i).updateDeviceName(devName[i]);
            }
        }

    }
}
